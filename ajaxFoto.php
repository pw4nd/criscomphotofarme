<?php

// Tempo {Quanto tempo leva o processamento}
$time_inicio = microtime(true);

// Configuração
$ativa_random = false;

// Array
$retorno = array();

if (file_exists("criscompFotos.json")) {
	
} else {
	
	$gJson = json_decode(file_get_contents("sistema.json"));
	
	// Configuração para fazer coleta os arquivos da imagens
	$pasta = $gJson->config->diretorio->pasta;
	$caminho = __DIR__ . "/{$pasta}/";

	// Se não existe na pasta então retorne o erro
	if (!is_dir($caminho)) {
		$retorno['erro']['codigo'] = "6111";
		$retorno['erro']['mensagem'] = "Pasta do {$pasta} não encontrada use o sistema de localização.";
	} else {

		$dir = new RecursiveDirectoryIterator($caminho);
		$ite = new RecursiveIteratorIterator($dir);
		$reg = new RegexIterator($ite, "/(^.+\.jpg$)/i", RecursiveRegexIterator::GET_MATCH);

		// Array vazio
		$arquivos = array();

		// For
		foreach ($reg as $value) {
			foreach ($value as $v) {
				$arquivo = str_replace($caminho, '', $v);
				// Evitar de duplicate
				if (!$v || in_array($arquivo, $arquivos)) {
					continue;
				}
				// Adiciona array do arquivos
				$arquivos[] = $arquivo;
			}
		}

		// Verificar se pra ativa o aleatorio ou não
		if ($ativa_random) {
			$retorno['arquivo'] = $arquivos[rand(0, (count($arquivos) - 1))];
		} else {
			if (isset($_GET['arq'])) {
				$num = (array_search($_GET['arq'], $arquivos) + 1);
				$nfoto = $arquivos[$num] ? $num : 0;
				$retorno['arquivo'] = $arquivos[$nfoto];
			} else {
				$retorno['arquivo'] = $arquivos[0];
			}
		}

		// Caminho arquivo
		$arq = $caminho . $retorno['arquivo'];

		// Coleta as informações
		$size = getimagesize($arq);
		$path = pathinfo($arq);

		// Retorno caminho para exibir a imagem
		$retorno['url'] = "{$pasta}/{$path['basename']}";

		// Gerar o retorno da imagens
		$retorno['size']['width'] = $size[0];
		$retorno['size']['height'] = $size[1];
		$retorno['size']['bits'] = $size['bits'];
		if (isset($_GET['debug'])) {
			$retorno['path']['dirname'] = $path['dirname'];
		}
		$retorno['path']['basename'] = $path['basename'];
		$retorno['path']['extension'] = $path['extension'];
		$retorno['path']['filename'] = $path['filename'];

		// Opcões
		$retorno['info']['aleatorio'] = $ativa_random;
		$retorno['info']['aleatorio'] = $ativa_random;
	}
	// Resultado do criscomp
	$retorno['criscomp']['sistema'] = "CriscomPhoto Frame";
	$retorno['criscomp']['versao'] = "1.0";
	$retorno['criscomp']['autor'] = "Cristiano Thomas";
	$retorno['criscomp']['email'] = "criscompbr@gmail.com";

	unset($retorno['arquivo']);

	// Final tempo
	$time_final = microtime(true);
	$time = $time_final - $time_inicio;
	$retorno['info']['time'] = $time;

	// Imprime na tela
	header('Content-Type: application/json');
	echo json_encode($retorno);
}