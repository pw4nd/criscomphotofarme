<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
	<head>
		<title>CriscomPhoto Frame versão 1.0</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/criscomp.js"></script>
		<link rel="stylesheet" href="css/criscomp.css" type="text/css" media="screen" />
	</head>
	<body>
		<div id="tela_cheia">
			<div id="barra_rodape">
				<div style="float: left; padding-left: 5px;"><span id="nome"></span><span id="tempo_de_vida_bebe"></span></div>
				<div style="float: right; padding-right: 5px; text-align: left; width: 250px;" id="dthr_atual"></div>
			</div>
			<img id="foto_1" class="foto" src="" width="100%" height="100%" />
			<img id="foto_2" class="foto" src="" width="100%" height="100%" />
		</div>
	</body>
</html>
