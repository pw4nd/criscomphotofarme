var $executar = false;

var criscompConfig = {
	$executar: false,
	$img: false,
	$arq: null,
	// Coleta JSON
	$tempo_screen: null,
	$tempo_dthr: null,
	$nome: null,
	$data_nascimento: null,
	$dthr_alteracao: null
};

var criscomp = {
	carregarSistema: function () {
		$.ajax({
			type: "GET",
			url: "sistema.json",
			error: function (e, ee) {
				window.console.log(e, ee);
			},
			success: function (r) {
				criscompConfig.$tempo_screen = r.config.tela.tempo_screen;
				if (r.config.tela.mostra_data_hora) {
					criscompConfig.$tempo_dthr = r.config.tela.tempo_dthr;
					criscomp.dthr();
				}
				if (r.config.tela.mostra_tempo_vida_bebe) {
					criscompConfig.$nome = r.vida_bebe.nome;
					criscompConfig.$data_nascimento = r.vida_bebe.data_nascimento;
					$("#nome").text(criscompConfig.$nome);
					criscomp.coletaDiaBebe();
				}
				criscompConfig.$dthr_alteracao = r.data.dthr_alteracao;
				setTimeout(criscomp.habilitar, 5000);
			}
		});
	},
	habilitar: function () {
		criscompConfig.executar = true;
		criscomp.screensaver();
	},
	desabilitar: function () {
		criscompConfig.executar = false;
	},
	screensaver: function () {
		var $foto_novo = "";
		var $foto_antigo = "";

		if (criscompConfig.$img) {
			criscompConfig.$img = false;
			$foto_novo = "foto_1";
			$foto_antigo = "foto_2";
		} else {
			criscompConfig.$img = true;
			$foto_novo = "foto_2";
			$foto_antigo = "foto_1";
		}
		$.ajax({
			type: "GET",
			url: "ajaxFoto.php",
			data: {arq: criscompConfig.$arq},
			error: function (e, ee) {
				window.console.log(e, ee);
			},
			success: function (r) {
				criscompConfig.$arq = r.path.basename;
				criscompConfig.$url = r.url;

				$('#' + $foto_novo).attr('src', criscompConfig.$url);
				var tmpImg = new Image();
				tmpImg.onload = function () {
					$('#' + $foto_antigo).fadeOut("slow", function () {
						$(this).attr("src", "");
					});
					$('#' + $foto_novo).fadeIn('slow');
					if (criscompConfig.executar) {
						setTimeout(function () {
							criscomp.screensaver();
						}, criscompConfig.$tempo_screen);
					}
				};
				tmpImg.src = $('#' + $foto_novo).attr('src');
			}
		});
	},
	dthr: function () {
		var d = new Date();
		var ano = d.getFullYear();
		var mes = (d.getMonth() + 1);
		var dia = d.getDay();
		var hora = d.getHours();
		var min = d.getMinutes();
		var seg = d.getSeconds();
		if (dia < 10) {
			dia = '0' + dia;
		}
		if (mes < 10) {
			mes = '0' + mes;
		}
		if (min < 10) {
			min = '0' + min;
		}
		if (seg < 10) {
			seg = '0' + seg;
		}
		$('#dthr_atual').html(dia + "/" + mes + "/" + ano + " " + hora + ":" + min + ":" + seg);
		setTimeout(criscomp.dthr, 1000);
	},
	coletaDiaBebe: function () {
		var data = criscompConfig.$data_nascimento.split('/');
		var $getData = criscompDate.calcularIdadeComp(data[0], data[1], data[2]);
		$("#tempo_de_vida_bebe").text(", tem " + $getData);
		setTimeout(criscomp.coletaDiaBebe, 1000);

	}
};

var criscompDate = {};

criscompDate.calcularIdadeComp = function (getDia, getMes, getAno) {
	// Data atual
	var dateObjAgora = new Date();
	var AgoraDia = dateObjAgora.getUTCDate();
	var AgoraMes = dateObjAgora.getUTCMonth() + 1;
	var AgoraAno = dateObjAgora.getUTCFullYear();
	// Coleta data para calcular
	var CalcDia = getDia;
	var CalcMes = getMes;
	var CalcAno = getAno;
	var retorno = "";
	var curd = new Date(AgoraAno, AgoraMes - 1, AgoraDia);
	var cald = new Date(CalcAno, CalcMes - 1, CalcDia);

	var dife = criscompDate.datadiff(curd, cald);
	retorno += (dife[0] ? dife[0] + " anos, " : "") + (dife[1] ? dife[1] + " meses e " : "") + dife[2] + " dias";

	return retorno;
};

criscompDate.datadiff = function (date1, date2) {
	var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
			y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();

	if (d1 < d2) {
		m1--;
		d1 += criscompDate.DiaNoMes(y2, m2);
	}
	if (m1 < m2) {
		y1--;
		m1 += 12;
	}
	return [y1 - y2, m1 - m2, d1 - d2];
};

criscompDate.DiaNoMes = function (Y, M) {
	with (new Date(Y, M, 1, 12)) {
		setDate(0);
		return getDate();
	}
};

criscomp.carregarSistema();